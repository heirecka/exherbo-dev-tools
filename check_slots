#!/usr/bin/env bash

# Copyright 2013 Benedikt Morbach <moben@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

return_code=0

usage() {
    echo "You need to pass at least one package name as a command line argument."
    echo "This script takes anything that 'cave print-ids -m' would take and that supports the 'install' action."
    echo "It returns a report for every matching package, indicating if one of the dependencies is slotted"
    echo "when the corresponding dependency specification doesn't have a slot operator."
    echo ""
    echo "It optionally takes some paramaters, which must be given _before_ the packages."
    echo "Parameters:"
    echo "   -h, --help           :  This text"
    echo "   -c, --no-magic       :  Turn colors and formatting off (useful for pasting)"
    echo "   -o, --only-broken    :  Only output packages with slot problems"
    echo "   -q, --quiet          :  Supress informational messages"
    echo "   -s, --slotted <spec> :  Only look at dependants of packages matching the given spec"
}

log() {
    if [[ "${quiet}" == off ]]; then
        echo "$*"
    fi
}

parsed_opts=$(
    getopt \
        --options hcoqs: \
        --long help,no-magic,only-broken,quiet,slotted: \
        --name check_slots \
        -- "$@"
)

# getopt failed and will print an error message for us. Add our usage text to it
if [ $? != 0 ] ; then echo "" ; usage ; exit 1 ; fi

eval set -- "${parsed_opts}"

# defaults
magic=on
show_all=on
quiet=off
slotted='*/*'

while true ; do
    case "${1}" in
        -h | --help ) usage ; exit ;;
        -c | --no-magic ) magic=off ; shift 1 ;;
        -o | --only-broken ) show_all=off ; shift 1 ;;
        -q | --quiet ) quiet=on ; shift 1 ;;
        -s | --slotted ) slotted=${2} ; shift 2 ;;
        -- ) shift 1 ; break ;;
        * ) break ;;
    esac
done

if [[ $magic == on ]] ; then
    default='\033[0m'
    red='\033[1;31m'
    green='\033[32m'
fi

if [[ $# -lt 1 ]] ; then
    usage
    exit 2
fi

log "Collecting all slotted packages..."

slotted_pkgs=(
    $(cave print-ids -s install                \
                     -m "${slotted}[.SLOT?]"   \
                     -m "${slotted}[.SLOT!=0]" \
                     -f '%c/%p\n'              )
)

log "Looking for slotting problems..."

for arg in "$@" ; do
    for fqdn in $(cave print-ids -s install -m "${arg}"'[.DEPENDENCIES?]') ; do
        nicely_slotted=yes
        problems=()

        [[ $magic == on ]] && echo -e -n "${fqdn} : checking..."

        # Get anything that looks like a category/package and doesn't have a slot dep (operator)
        # The allowed characters are taken from gentoo PMS. I assume exheres-0 didn't change those.
        fqdn_deps=$(cave print-id-metadata --format '%v' --raw-name DEPENDENCIES "=${fqdn}" \
                  | grep -E '[A-Za-z0-9+_.-]*/[A-Za-z0-9+_.-]*( |\[)' -o | sed -e 's#.$##g' )

        for dep in ${fqdn_deps} ; do
            for slotted in "${slotted_pkgs[@]}" ; do
                if [[ ${slotted} == ${dep} ]] ; then
                    nicely_slotted=no
                    problems+=( ${dep} )
                    break
                fi
            done
        done

        # We are going to rewrite the whole line in pretty colors now
        [[ $magic == on ]] && echo -n -e '\r\033[K'

        if [[ $nicely_slotted == yes ]] ; then
            [[ $show_all == on ]] && echo -e "${green}${fqdn}${default} : Everything alright"
        else
            echo -e "${red}${fqdn}${default} : Found some slotting problems:"
            for prob in "${problems[@]}" ; do
                echo "    ${prob}"
            done
            return_code=100
        fi
    done
done

exit $return_code
